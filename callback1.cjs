const boardData = require("./data/boards_2.json");

let boardDetails = {};
function getBoardDatabyID(boardID, callback) {
  setTimeout(
    () => {
      boardDetails = boardData.filter((board) => board.id == boardID);
      callback(null, boardDetails);
    },
    2000,
    boardID
  );
}

module.exports = getBoardDatabyID;
