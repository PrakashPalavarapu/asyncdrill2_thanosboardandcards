const listData = require("./data/lists_1.json");

function getBoardListByBoardId(boardID, callback) {
  setTimeout(() => {
    let boardList =
      listData[Object.keys(listData).filter((board) => board == boardID)];
    callback(null, boardList);
  }, 2000);
}
module.exports = getBoardListByBoardId;
