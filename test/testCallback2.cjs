const getBoardListByBoardId = require("../callback2.cjs");

getBoardListByBoardId("mcu453ed", (err, listData) => {
  try {
    if (err) {
      throw new Error("failed loading board list");
    } else {
      console.log(listData);
    }
  } catch (error) {
    console.error(error);
  }
});
