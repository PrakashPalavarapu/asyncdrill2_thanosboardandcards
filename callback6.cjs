const getBoardInfo = require("./callback1.cjs");
const getBoardList = require("./callback2.cjs");
const getCardInfo = require("./callback3.cjs");
const boardsData = require("./data/boards_2.json");

function getThanosAllCards() {
  let thanosId = boardsData.filter((board) => board.name == "Thanos")[0].id;

  // Get information from the Thanos boards
  getBoardInfo(thanosId, (err, thanosInfo) => {
    try {
      if (err) {
        throw new Error("failed at reading getBoardinfo");
      } else {
        console.log("BoardInfo - Thanos Information");
        console.log(thanosInfo);

        // Get all the lists for the Thanos board
        getBoardList(thanosId, (err1, thanosList) => {
          try {
            if (err1) {
              throw new Error("failed at reading getBoardList");
            } else {
              console.log("ListData - Thanos Lists");
              console.log(thanosList);

              // Get all cards for the Mind and Space list
              let stoneIds = thanosList.map((stoneData) => {
                return stoneData.id;
              });
              for (let stone of stoneIds) {
                getCardInfo(stone, (err2, cardData) => {
                  try {
                    if (err2) {
                      throw new Error("failed at reading getCardinfo");
                    } else {
                      console.log(cardData);
                    }
                  } catch (err) {
                    console.error(err.name, err.message);
                  }
                });
              }
            }
          } catch (err) {
            console.error(err.name, err.message);
          }
        });
      }
    } catch (err) {
      console.error(err.name, err.message);
    }
  });
}
module.exports = getThanosAllCards;
