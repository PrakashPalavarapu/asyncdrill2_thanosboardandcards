const cardsData = require("./data/cards_2.json");

function getCardsDataByListId(listID, callback) {
  setTimeout(() => {
    let card =
      cardsData[Object.keys(cardsData).filter((list) => list == listID)];
    callback(null, card);
  }, 2000);
}

module.exports = getCardsDataByListId;
