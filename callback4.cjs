const getBoardInfo = require("./callback1.cjs");
const getBoardList = require("./callback2.cjs");
const getCardInfo = require("./callback3.cjs");
const boardsData = require("./data/boards_2.json");

function getThanosMindStone() {
  let thanosId = boardsData.filter((board) => board.name == "Thanos")[0].id;

  // Get information from the Thanos boards
  getBoardInfo(thanosId, (err, thanosInfo) => {
    try {
      if (err) {
        throw new Error("failed at calling getBoardInfo");
      } else {
        console.log("BoardInfo - Thanos Information");
        console.log(thanosInfo);

        // Get all the lists for the Thanos board
        getBoardList(thanosId, (err1, thanosList) => {
          try{
            if (err1) {
              throw new Error("failed at reading getBoardList");
            } else {
              console.log("ListData - Thanos Lists");
              console.log(thanosList);

              // Get all cards for the Mind list simultaneously
              let mindStoneId = thanosList.filter(
                (stone) => stone.name == "Mind"
              )[0].id;
              getCardInfo(mindStoneId, (err2, mindStoneCardDetails) => {
                try{if (err2) {
                  throw new Error("failed at reading getardInfo");
                } else {
                  console.log(mindStoneCardDetails);
                }}catch(error){
                  console.error(error.name, error.message);
                }
              });
            }
          }catch(error){
            console.error(err1.name, err1.message);
          }
        });
      }
    } catch (error) {
      console.error(err.name, err.message);
    }
  });
}
module.exports = getThanosMindStone;
